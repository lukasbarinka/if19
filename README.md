# Shell Patterns Vs Regular Expressions

## Table of contents

- Shell Patterns
- Regular Expressions
- Extended Shell Patterns
- Syntax of Shell Patterns Vs Regular Expression
- Usage of Patterns and Expressions

## Video presentation

https://www.youtube.com/watch?v=dOKydwMDYUU