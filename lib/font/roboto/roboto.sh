for font in Roboto*.ttf
do
font=${font%.*}
cat <<FACE
@font-face {
    font-family: 'Roboto';
    src: url('$font.eot');
    src: url('$font.eot?#iefix') format('embedded-opentype'),
         url('$font.woff') format('woff'),
         url('$font.woff2') format('woff2'),
         url('$font.ttf') format('truetype');
    font-weight: .00;
    font-style: normal;
    font-style: italic;
}
FACE
done
